<div class="registerform">
    <h3>Zarejestruj się aby mikroblogować</h3>
    <form method="POST" action='/~p19/intro2.wsgi//register'>
        <div class="form-item">
            <label>Login</label>
            <input type="text" name="login" required />
        </div>
        <div class="form-item">
            <label>Hasło</label>
            <input type="password" name="pass" required />
        </div>
        <div class="form-item">
            <label>E-mail</label>
            <input type="email" name="email" required />
        </div>
        <input type="submit" value="Zarejestruj się" />
    </form>
</div>
%rebase maincontent_blocks title=title, logged_in=logged_in