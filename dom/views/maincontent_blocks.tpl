%def leftmenu():
    %if logged_in == 0:
        <ul>
            <li><a href="/~p19/intro2.wsgi/">Wpisya</a></li>
            <li><a href="/~p19/intro2.wsgi/login">Logowaniee</a></li>
            <li><a href="/~p19/intro2.wsgi/register">Rejestracja</a></li>
            <li><a href="/~p19/intro2.wsgi/kontakt">Kontakt</a></li>
        </ul>
    %end
    %if logged_in != 0:
        <ul>
            <li><a href="/~p19/intro2.wsgi/">Wpisy</a></li>
            <li><a href="/~p19/intro2.wsgi/logout">Wyloguj</a></li>
            <li><a href="/~p19/intro2.wsgi/addbottle">Dodaj wpisa</a></li>
            <li><a href="/~p19/intro2.wsgi/kontakt">Kontakt</a></li>
        </ul>
     %end
%end
%def maincontent():
    <div class="content">
        %include
    </div>
%end

%rebase maincontent leftmenu=leftmenu, maincontent=maincontent, title=title